<?php
$target_dir = "../configs/";
$target_file = basename($_FILES["fileToUpload"]["name"]);
$target_path = $target_dir . $target_file;

if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_path)) {
    echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    $output = shell_exec("../scripts/slicer-req.sh $target_file");
    echo $output;
} else {
    echo "Sorry, there was an error uploading your file.";
}

?>